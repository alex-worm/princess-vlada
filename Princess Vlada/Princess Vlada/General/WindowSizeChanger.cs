﻿using System;

namespace Princess_Vlada
{
    public class WindowSizeChanger
    {
        public void ChangeSize(int width, int height)
        {
            Console.SetWindowSize(width, height);
            Console.SetBufferSize(width, height);
        }
    }
}
