﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Princess_Vlada
{
    public class BeforeActionResult
    {
        public ActionType Action { get; set; }

        public int Damage { get; set; }

        public bool IsCanMove { get; set; }

        public bool IsDamaged { get; set; }

        public bool IsGameOver { get; set; }

        public bool IsGameWone { get; set; }
    }
}
