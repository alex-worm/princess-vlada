﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Princess_Vlada
{
    public static class DisplayerSettings
    {
        public static Dictionary<string, DisplayEntityParams> EntityParams = new Dictionary<string, DisplayEntityParams>
        {
            {"wall", new DisplayEntityParams{ Symbol = 'X', Color = ConsoleColor.Gray } },
            {"player", new DisplayEntityParams{ Symbol = 'P', Color = ConsoleColor.Cyan } },
            {"default-trap", new DisplayEntityParams{ Symbol ='_', Color = ConsoleColor.DarkGray } },
            {"hp-bar", new DisplayEntityParams{ Color = ConsoleColor.Red } },
            {"floor", new DisplayEntityParams{ Symbol = '_', Color = ConsoleColor.DarkGray } },
            {"princess", new DisplayEntityParams{ Symbol = 'V', Color = ConsoleColor.Magenta } }
        };

        public static int TopIndent = 2;
    }
}
