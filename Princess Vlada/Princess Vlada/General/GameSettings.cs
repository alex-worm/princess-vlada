﻿namespace Princess_Vlada
{
    public abstract class GameSettings
    {
        private Position _startPlayerPosition;

        public int FieldHeight { get; set; }

        public int FieldWidth { get; set; }

        public int TrapsNumber { get; set; }

        public int MaxTrapDamage { get; set; }

        public int PlayerHP { get; set; }

        public Position PrincessPosition { get; set; }

        public Position StartPosition
        {
            get => _startPlayerPosition;

            set
            {
                _startPlayerPosition.Column = 0;

                _startPlayerPosition.Row = 0;
            }
        }

        public abstract void CreateGameWindow();
    }
}
