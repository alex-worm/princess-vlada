﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Princess_Vlada
{
    public class DisplayEntityParams
    {
        public char Symbol { get; set; }

        public ConsoleColor Color { get; set; }
    }
}
