﻿namespace Princess_Vlada
{
    public class Position
    {
        public int Row { get; set; }

        public int Column { get; set; }
    }
}
