﻿using System;

namespace Princess_Vlada
{
    public class Movement
    {
        public ActionType GetAction()
        {
            return Console.ReadKey(true).Key switch
            {
                ConsoleKey.UpArrow => ActionType.MoveUp,
                ConsoleKey.RightArrow => ActionType.MoveRight,
                ConsoleKey.DownArrow => ActionType.MoveDown,
                ConsoleKey.LeftArrow => ActionType.MoveLeft,
                _ => ActionType.HoldIt,
            };
        }

        public bool IsNewGameRequested()
        {
            while (true)
            {
                switch (Console.ReadKey(true).Key)
                {
                    case ConsoleKey.Enter:
                        return true;
                    case ConsoleKey.Escape:
                        return false;
                    default:
                        continue;
                }
            }
        }
    }
}
