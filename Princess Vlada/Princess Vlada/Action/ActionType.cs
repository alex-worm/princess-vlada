﻿namespace Princess_Vlada
{
    public enum ActionType
    {
        MoveUp,
        MoveRight,
        MoveDown,
        MoveLeft,
        HoldIt
    }
}
