﻿using System;

namespace Princess_Vlada
{
    public class PrincessVladaSettings : GameSettings
    {
        private const int PlayerHpWidth = 20;

        public override void CreateGameWindow()
        {
            Console.CursorVisible = false;

            var window = new WindowSizeChanger();

            window.ChangeSize(FieldWidth + PlayerHpWidth, FieldHeight);
        }

        public void ResetSettings()
        {
            Console.CursorVisible = true;
            Console.Clear();
            Console.SetCursorPosition(0, 0);
        }
    }
}
