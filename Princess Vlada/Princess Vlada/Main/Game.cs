﻿using System;
using System.Collections.Generic;
using System.Text;


namespace Princess_Vlada
{
    public class Game
    {
        private bool _isPlayerWon = false;
        private bool _isGameOn;
        private Displayer _displayer;
        private Movement _move;

        private GameSettings Settings { get; set; }
        private Player Player { get; set; }
        private Princess Princess { get; set; }
        private Field Field { get; set; }

        public Game(Movement move, Displayer drawer, GameSettings settings)
        {
            _move = move ?? throw new NullReferenceException(nameof(move));
            _displayer = drawer ?? throw new NullReferenceException(nameof(drawer));
            Settings = settings ?? throw new NullReferenceException(nameof(settings));
        }

        public void Launch()
        {
            _isGameOn = true;
            Console.Clear();
            try
            {
                InitializeGame();

                while (_isGameOn)
                {
                    var actionResult = GetActionResult(_move.GetAction());

                    UpdateCurrentPayerHP(actionResult);
                    UpdateCurrentPlayerPosition(actionResult);
                    CheckIsWon(actionResult);
                }

                ShowGameEndMenu();
            }
            catch (Exception ex)
            {
                this._displayer.DisplayErrorLog($"The application crashed.\nReason : {ex.Message}");
            }
        }

        private void InitializeGame()
        {
            Player = new Player
            {
                Position = new Position
                {
                    Column = 1,
                    Row = 1
                },
                HP = Settings.PlayerHP
            };

            Princess = new Princess
            {
                Position = Settings.PrincessPosition
            };

            Settings.CreateGameWindow();

            Field = new Field(Settings, Player, Princess);
            _displayer.DisplayAll(Field, Player);
        }

        private BeforeActionResult GetActionResult(ActionType actionType)
        {
            int verticalStep = 0;
            int horizontalStep = 0;

            switch (actionType)
            {
                case ActionType.MoveUp:
                    verticalStep = -1;
                    break;
                case ActionType.MoveRight:
                    horizontalStep = +1;
                    break;

                case ActionType.MoveDown:
                    verticalStep = +1;
                    break;

                case ActionType.MoveLeft:
                    horizontalStep = -1;
                    break;

                default:
                    break;
            }

            return Field.GetActionResult(new Position
            {
                Row = Player.Position.Row + verticalStep,
                Column = Player.Position.Column + horizontalStep
            }, actionType);
        }

        private void UpdateCurrentPayerHP(BeforeActionResult actionResult)
        {
            if (!actionResult.IsDamaged)
            {
                return;
            }

            this.Player.HP -= actionResult.Damage;

            if (Player.HP < 1)
            {
                this._isGameOn = false;
                this._isPlayerWon = false;
            }
            this._displayer.DisplayHP(this.Player.HP);
        }

        private void UpdateCurrentPlayerPosition(BeforeActionResult actionResult)
        {
            if (!actionResult.IsCanMove)
            {
                return;
            }

            switch (actionResult.Action)
            {
                case ActionType.MoveUp:
                    Field.RegisterMovement(
                        Player.Position,
                        new Position
                        {
                            Row = Player.Position.Row - 1,
                            Column = Player.Position.Column
                        },
                        _displayer.Display);
                    break;

                case ActionType.MoveRight:
                    Field.RegisterMovement(Player.Position, new Position
                    {
                        Row = Player.Position.Row,
                        Column = Player.Position.Column + 1
                    },
                        _displayer.Display);
                    break;

                case ActionType.MoveDown:
                    Field.RegisterMovement(Player.Position, new Position
                    {
                        Row = Player.Position.Row + 1,
                        Column = Player.Position.Column
                    },
                        _displayer.Display);
                    break;

                case ActionType.MoveLeft:
                    Field.RegisterMovement(Player.Position, new Position
                    {
                        Row = Player.Position.Row,
                        Column = Player.Position.Column - 1
                    },
                        _displayer.Display);
                    break;
            }
        }

        private void CheckIsWon(BeforeActionResult actionResult)
        {
            if (actionResult.IsGameWone)
            {
                _isGameOn = false;
                _isPlayerWon = true;
            }
        }

        private void ShowGameEndMenu()
        {
            _displayer.DisplayMessageForUser((_isPlayerWon ? "Congratulations on the victory!!" : "Losing :( Maybe one more time?")
                + "\nRepeat - press Enter \nExit - press Esc");
            if (_move.IsNewGameRequested())
            {
                Launch();
            }
        }
    }
}
