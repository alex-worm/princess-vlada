﻿namespace Princess_Vlada
{
    class Program
    {
        static void Main(string[] args)
        {

            var settings = new PrincessVladaSettings
            {
                TrapsNumber = 10,
                MaxTrapDamage = 10,
                FieldHeight = 10,
                FieldWidth = 10,
                PlayerHP = 10,
                PrincessPosition = new Position() { Row = 10, Column = 10 }
            };

            settings.FieldHeight += 2;
            settings.FieldWidth += 2;

            var game = new Game(
                new Movement(),
                new Displayer(settings.FieldHeight + 2, settings.FieldWidth + 2),
                settings);

            game.Launch();
        }
    }
}
