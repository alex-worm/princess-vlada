﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Princess_Vlada
{
    public class Displayer
    {
        private const string CleanerString = "   ";

        private int _gameHeight;
        private int _gameWidth;

        public Displayer(int height, int width)
        {
            _gameHeight = height;
            _gameWidth = width;
        }

        public void DisplayAll(Field displayZone, Player player)
        {
            foreach (var cell in displayZone.Cells)
            {
                DisplayChar(GetMemberChar(cell.Member), cell.Position, GetMemberColor(cell.Member));
            }

            DisplayTitle();
            DisplayHP(player.HP);
        }

        private void DisplayChar(char sym, Position position, ConsoleColor color)
        {
            if (position == null)
            {
                throw new NullReferenceException(nameof(position));
            }

            Console.ForegroundColor = color;
            Console.SetCursorPosition(position.Column, position.Row);
            Console.Write(sym);
            Console.ResetColor();
        }

        private char GetMemberChar(ZoneMember member)
        {
            if (member == null)
            {
                return DisplayerSettings.EntityParams["floor"].Symbol;
            }

            switch (member)
            {
                case Wall _:
                    return DisplayerSettings.EntityParams["wall"].Symbol;
                case Trap _:
                    return DisplayerSettings.EntityParams["default-trap"].Symbol;
                case Player _:
                    return DisplayerSettings.EntityParams["player"].Symbol;
                case Princess _:
                    return DisplayerSettings.EntityParams["princess"].Symbol;
            }

            return '?';
        }

        private ConsoleColor GetMemberColor(ZoneMember member)
        {
            if (member is Wall)
            {
                return DisplayerSettings.EntityParams["wall"].Color;
            }
            else if (member is Trap)
            {
                return DisplayerSettings.EntityParams["default-trap"].Color;
            }
            else if (member is Player)
            {
                return DisplayerSettings.EntityParams["player"].Color;
            }
            else if (member is Princess)
            {
                return DisplayerSettings.EntityParams["princess"].Color;
            }

            return DisplayerSettings.EntityParams["floor"].Color;
        }

        private void DisplayTitle()
        {
            var position = new Position() { Row = 0, Column = _gameWidth + 2 };
            Console.ForegroundColor = ConsoleColor.Green;
            Console.SetCursorPosition(position.Column, position.Row);
            Console.Write("Princess Vlada");
            Console.ResetColor();
        }

        public void DisplayHP(int hpAmount)
        {
            var position = new Position() { Row = DisplayerSettings.TopIndent, Column = _gameWidth + 2 };
            Console.SetCursorPosition(position.Column, position.Row);
            Console.ForegroundColor = DisplayerSettings.EntityParams["hp-bar"].Color;
            Console.Write($"HP : {hpAmount}{CleanerString}");
            Console.ResetColor();
        }

        public void DisplayMessageForUser(string message)
        {
            Console.Clear();
            Console.WriteLine(message);
        }

        public void DisplayErrorLog(string message)
        {
            Console.Clear();
            Console.WriteLine(message);
            Console.ReadKey();
        }

        public void Display(Position position, ZoneMember member)
        {
            DisplayChar(GetMemberChar(member), position, GetMemberColor(member));
        }
    }
}
