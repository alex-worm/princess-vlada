﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Princess_Vlada
{
    public class Field
    {
        private const int MinWidth = 10;
        private const int MinHeight = 10;

        private Cell[,] _cell;
        private Spawner _spawner;

        public Cell[,] Cells
        {
            get
            {
                return _cell;
            }
            set
            {
                _cell = value.GetLength(0) >= MinHeight && value.GetLength(1) >= MinWidth
                    ? value
                    : new Cell[MinHeight, MinWidth];
            }
        }

        public Field(GameSettings settings, Player player, Princess vlada)
        {
            Cells = new Cell[settings.FieldHeight, settings.FieldWidth];

            _spawner = new Spawner(Cells);

            InitializeCells();

            _spawner.SpawnTerritory();
            _spawner.SpawnTraps(settings.TrapsNumber, settings.MaxTrapDamage);
            _spawner.SpawnPlayer(player);
            _spawner.SpawnPrincess(vlada);
        }

        private void InitializeCells()
        {
            for (int i = 0; i < Cells.GetLength(0); i++)
            {
                for (int j = 0; j < Cells.GetLength(1); j++)
                {
                    Cells[i, j] = new Cell(i, j);
                }
            }
        }

        public BeforeActionResult GetActionResult(Position position, ActionType actionType)
        {
            if (position == null)
            {
                return new BeforeActionResult();
            }

            var member = GetMember(position);

            return new BeforeActionResult
            {
                Action = actionType,
                IsCanMove = member == null || member is Trap ? true : false,
                IsDamaged = member != null && member is Trap ? true : false,
                IsGameWone = member != null && member is Princess ? true : false,
                Damage = (member != null && member is Trap trap) ? trap.Damage : default
            };
        }

        public void RegisterMovement(Position currentPosition, Position newPosition, Action<Position, ZoneMember> displayAfterMove)
        {
            if (GetMember(currentPosition) is Player player)
            {
                player.Position = newPosition;
            }

            var currentCell = _cell[currentPosition.Row, currentPosition.Column];
            var targetСell = _cell[newPosition.Row, newPosition.Column];

            targetСell.Member = currentCell.Member;
            currentCell.Member = null;

            displayAfterMove(currentPosition, currentCell.Member);
            displayAfterMove(newPosition, targetСell.Member);
        }

        private ZoneMember GetMember(Position memberPosition)
        {
            if (memberPosition == null)
            {
                return null;
            }

            return Cells[memberPosition.Row, memberPosition.Column].Member;
        }
    }
}
