﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Princess_Vlada
{
    public class Trap : ZoneMember
    {
        public int Damage { get; set; }

        public new Position Position { get; set; }
    }
}
