﻿using System;

namespace Princess_Vlada
{
    public class Spawner
    {
        private Cell[,] _targetCells;
        private Random _randomizer;

        public Spawner(Cell[,] cells)
        {
            _targetCells = cells;
            _randomizer = new Random();
        }

        public void SpawnTerritory()
        {
            var zoneLength = _targetCells.GetLength(0);
            var zoneWidth = _targetCells.GetLength(1);

            for (int i = 0; i < zoneLength; i++)
            {
                for (int j = 0; j < zoneWidth; j++)
                {
                    if (i == 0
                        || i == zoneLength - 1
                        || j == 0
                        || j == zoneWidth - 1)
                    {
                        _targetCells[i, j].Member = new Wall();
                        continue;
                    }
                }
            }
        }

        public void SpawnPlayer(Player player)
        {
            _targetCells[player.Position.Row, player.Position.Column].Member = player;
        }

        public void SpawnPrincess(Princess princess)
        {
            _targetCells[princess.Position.Row, princess.Position.Column].Member = princess;
        }

        public void SpawnTraps(int trapsNumber, int maxTrapDamage)
        {
            for (int i = 0; i < trapsNumber; i++)
            {
                var row = _randomizer.Next(1, _targetCells.GetLength(0) - 1);
                var column = _randomizer.Next(1, _targetCells.GetLength(1) - 1);

                if (!(_targetCells[row, column].Member is Player 
                    || _targetCells[row, column].Member is Trap 
                    || _targetCells[row, column].Member is Princess))
                {
                    _targetCells[row, column].Member = new Trap() { Damage = _randomizer.Next(1, maxTrapDamage + 1) };
                }
            }
        }
    }
}
