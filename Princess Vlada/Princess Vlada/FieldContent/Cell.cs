﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Princess_Vlada
{
    public class Cell
    {
        public Position Position { get; set; }

        public Cell(int row, int column)
        {
            this.Position = new Position
            {
                Row = row,
                Column = column
            };
        }

        public ZoneMember Member { get; set; }
    }
}
